project demo
============

a collection of roboframework and python scripts for test



usage example

given a docker data container : project_data_1  with a /tests volume


launch a shell to manage project repository

```

docker run -ti --rm --volumes-from project_data_1 cocoon/base /bin/bash


cd /tests
git clone https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/project_demo.git


```

