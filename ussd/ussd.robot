*** Settings ***
Documentation
...

Resource   /tests/project_demo/library/phone_lib.robot



*** Variables ***
${Alice}=    482cf4c2
${Alice_number}=     0688935353

${Bob}=    8be038bc
${Bob_number}=       0631337126


*** Keywords ***


Macro ussd wait message
    [arguments]     ${user}    ${timeout}=3000
   [Documentation]    wait to see the dialog message
   #dialog_id = 'com.android.phone:id/dialog_message'
   #wait = d1.device(resourceId=dialog_id).wait.exists(timeout=3000)

    ${msg}     Wait For Exists   ${user}   timeout=${timeout}  resourceId=com.android.phone:id/dialog_message
    Return From Keyword If    ${msg} == True
    Fail    msg=no ussd message received

Macro ussd display message
    [arguments]     ${user}    ${timeout}=3000    ${resourceId}=com.android.phone:id/dialog_message
   [Documentation]    display ussd message

    ${msg}     Wait For Exists   ${user}   timeout=${timeout}  resourceId=${resourceId}
    Return From Keyword If    ${msg} == True
    Fail    msg=no ussd message received


Macro ussd select choice
    [arguments]    ${user}    ${choice}

    Set Text    ${user}   input_text=${choice}    resourceId=com.android.phone:id/input_field


    # select choice 1: detail suivi conso
    #input_field = d1.device( resourceId ='com.android.phone:id/input_field')
    #input_field.long_click()

    # input choice
    #input_field.set_text('1')


    # send
    #send_button = d1.device(resourceId='android:id/button1')
    #send_button.click()
    click    ${user}    resourceId=android:id/button1


    # wait next screen
    #d1.device.wait.update()
    Wait Update    ${user}

Macro ussd select cancel
    [arguments]    ${user}
    [documentation]    press cancel
    #cancel_button = d1.device(resourceId='android:id/button2')
    #cancel_button.click()
    click    ${user}    resourceId=android:id/button2


Unit ussd_123
    [arguments]     ${user}
    [Documentation]
    Open Session    ${user}

    Macro Home  ${user}
    Macro Quick Launch Phone    ${user}
    Macro Phone Keypad  ${user}
    Macro Phone Dial Number     ${user}    "#123#"


    # main menu: select choice 1: detail suivi conso
    Macro ussd wait message    ${user}
    Macro ussd select choice    ${user}    "1"

	# menu suivi conso:  select 1: appels
    Macro ussd select choice    ${user}    "1"

    # print message conso appel
    Macro ussd display message    ${user}

    Builtin.Sleep   3


    # select 0 -retour
    Macro ussd select choice    ${user}    "0"


    # select 8 -retour
    Macro ussd select choice    ${user}    "8"

    # click cancel button
    Macro ussd select cancel    ${user}





    #Macro Phone hangup call     ${user}

    Press Home  ${user}


    [teardown]  Close Session



Unit ussd_125
    [arguments]     ${user}    ${timeout}=3000
    [Documentation]  ussd 125 get orange wifi access token
    Open Session    ${user}

    Macro Home  ${user}
    Macro Quick Launch Phone    ${user}
    Macro Phone Keypad  ${user}
    Macro Phone Dial Number     ${user}    "#125#"


    # get the message USSD code running
    #message = d1.device(resourceId= 'android:id/message', className = 'android.widget.TextView').text
    #display( message)
    #${is_widget}  Wait For Exists  ${user}  timeout=${timeout}  resourceId=android:id/message className=android.widget.TextView
    #Should Be True    ${is_widget}

   # wait to see OK button
    #d1.device(resourceId='android:id/button1').wait.exists(timeout=3000)
    ${is_button}  Wait For Exists  ${user}  timeout=${timeout}  resourceId=android:id/button1
    Should Be True    ${is_button}
    # we got the screen

    # get the message
    #message = d1.device(resourceId= 'android:id/message', className = 'android.widget.TextView').text
    #display( message )
    #${is_widget}  Wait For Exists  ${user}  timeout=${timeout}  resourceId=android:id/message className=android.widget.TextView
    #Should Be True    ${is_widget}

    # wait to see message
     Builtin.Sleep   5


    # press OK
    #button = d1.device(resourceId='android:id/button1')
    #button.click()
    click    ${user}    resourceId=android:id/button1


    Press Home  ${user}


    [teardown]  Close Session






*** Test Cases ***


connected devices
    [Documentation]     list connected devices
    macro adb devices


ussd_123
    Unit ussd_123     ${Alice}

#ussd_125
#    Unit ussd_125     ${Bob}


# simple_call
#     [Documentation]     userA call userB , userB answers call , sleep 2 , userA hangups
#     Unit Simple Call    ${Alice}    ${Bob}  ${Bob_number}
