from fabric.api import local , cd , settings

root = "/tests"
project_name = "project_demo"
project_url = "https://cocoon_bitbucket@bitbucket.org/cocoon_bitbucket/project_demo.git" 


def pull():
	# git clone or pull of the project
	with cd( root ):
    	
		with settings(warn_only=True):
		    if local( "test -d %s/%s" % (root,project_name) ).failed:
		        local("git clone %s" % project_url)
		    else:
				with cd( "%s" % project_name ):
				    local("git pull")
				    