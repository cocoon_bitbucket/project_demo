# robot framework test

*** Settings ***
Documentation     base robotframework library for droydrunner
    ...


Library 	droydclient.robot_plugin.Pilot
Library     Collections


*** Keywords ***

#
#  adb macros
#
Macro adb devices
   [Documentation]  display list of connectd devices

    ${devices}  Adb devices
    Log List    ${devices}
    return from Keyword     ${devices}


#
#   general device macros
#

Macro Unlock Screen
    [arguments]     ${user}
    [Documentation]     unlock screen if necessary

    Turn On Screen  ${user}
    # get current screen
    ${device_info}  Get Device Info     ${user}
    #Log Dictionary  ${device_info}
    ${package_name}     Get From Dictionary     ${device_info}  currentPackageName
    Log     ${package_name}
    # return if not keyguard screen
    Return From Keyword If    '${package_name}' != 'com.android.keyguard'
    # swipe to unlock device
    Swipe By Coordinates   ${user}     sx=270     sy=1440    ex=800     ey=1000    steps=50


Macro Home
    [arguments]     ${user}
    [Documentation]     return to home screen
    Macro Unlock Screen     ${user}
    Press Home   ${user}
    Press Home   ${user}

Macro Quick Launch Phone
    [arguments]     ${user}
    [Documentation]     launch phone application from home screen with hot key
    click   ${user}  className=android.widget.TextView     packageName=com.sec.android.app.launcher    text=Phone


