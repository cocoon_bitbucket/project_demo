*** Settings ***
Documentation
...

Resource   /tests/project_demo/library/droyd_lib.robot



*** Variables ***
${Alice}=    482cf4c2
${Alice_number}=     0688935353

${Bob}=    8be038bc
${Bob_number}=       0631337126



${package_launcher}=  com.sec.android.app.launcher
${package_cineday}=  fr.orange.cineday

${cineday_text}=  Cinéday


*** Keywords ***


Macro launch cineday
    [arguments]    ${user}
    [Documentation]   from home menu enter cineday application ( icon must be in home screen )

    # goto home screen
    Macro Home  ${user}

    # click on cineday icon
    Click    ${user}    packageName=${package_launcher}  text=${cineday_text}

    #  wait splash screen
    Wait Update    ${user}


Macro cineday main tab
    [arguments]    ${user}    ${timetout}=3000
    [Documentation]   wait for main tab: films/salles /mes codes/rechercher
    ${msg}     Wait For Exists   ${user}   timeout=${timeout}  resourceId=android:id/tabs packageName=${package_cineday}
    Return From Keyword If    ${msg} == True
    Fail    msg=no ussd message received


	# wait for  main tab (films/salles /mes codes/rechercher)
	#main_tab_selector = d.device( resourceId="android:id/tabs", packageName= package_cineday)
	#ok = main_tab_selector.wait(2000)



Macro cineday click on salles
    [arguments]    ${user}
    [Documentation]  from main tab click on salles tab
    Click    ${user}     index=1  className=android.widget.RelativeLayout
    #Wait Update  ${user}


Macro cineday click on rechercher
    [arguments]    ${user}
    [Documentation]  from main tab click on rechercher tab
    Click    ${user}     index=3  className=android.widget.RelativeLayout
    Wait Update  ${user}




Macro cineday click on films
    [arguments]    ${user}
    [Documentation]  from main tab click on films tab
    Click    ${user}     index=0  className=android.widget.RelativeLayout
    #Wait Update  ${user}
    #films_selector = main_tab_selector.child(index= '0',className ="android.widget.RelativeLayout")

Macro cineday click on sorties
    [arguments]    ${user}
    [Documentation]  from films screen click on tab: sorties
    Click    ${user}    resourceId=fr.orange.cineday:id/btn_films_sorties

Macro cineday click on affiche
    [arguments]    ${user}
    [Documentation]  from films screen click on tab: affiche
    Click    ${user}    resourceId=fr.orange.cineday:id/btn_films_affiche

Macro cineday click on prochainement
    [arguments]    ${user}
    [Documentation]  from films screen click on tab: prochainement
    Click    ${user}    resourceId=fr.orange.cineday:id/btn_films_prochainement


Macro cineday select film by index
    [arguments]    ${user}  ${index}=0
    [Documentation]  from films screen click on tab: prochainement
    Click   ${user}   resourceId=fr.orange.cineday:id/films_listview  scrollable=True
    # select first film
    #first_film = film_list_view.child(resourceId="fr.orange.cineday:id/films_row" , index =0)
    #first_film.click()
    Wait Update  ${user}



Unit basic cineday demo
    [arguments]    ${user}
    [Documentation]  navigate into orange cineday application

    Open Session    ${user}

    Macro Home  ${user}

    Macro launch cineday    ${user}

    #Macro cineday click on salles  ${Alice}

    Macro cineday click on films  ${user}

    Macro cineday click on affiche  ${user}

    Macro cineday click on prochainement  ${user}


    Macro cineday click on sorties  ${user}

    #Macro cineday select film by index  ${Alice}  index=0

    Builtin.sleep  2

    Press Home  ${user}


    [teardown]  Close Session





*** Test Cases ***


connected devices
    [Documentation]     list connected devices
    macro adb devices


basic cineday demo
    [Documentation]  navigate into orange cineday application

    Unit basic cineday demo   ${Alice}



